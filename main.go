package main

import (
	"fmt"
	"time"

	"gitlab.com/wayarmy/wayarmy-go/cmd"
)

const timeout = time.Minute * 5

func main() {
	start := time.Now()
	end := start

	for end.Sub(start) < timeout {
		req := cmd.New()
		respCode, respTime, err := req.GetRequestResponseTime()
		if err != nil {
			fmt.Println(err)
			fmt.Print("Response Time:")
		}
		fmt.Println("Response code: ", respCode)
		fmt.Println("Reponse Time: ", respTime)
		fmt.Println("----------------------------------")
		time.Sleep(1 * time.Second)
		end = time.Now()
	}
}
