package cmd

import (
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
)

var (
	mux     *http.ServeMux
	server  *httptest.Server
	request *Request
)

func setUp() {
	mux = http.NewServeMux()
	server = httptest.NewServer(mux)
	request = New()
}

func tearDown() {
	server.Close()
}

func assertEqual(t *testing.T, result interface{}, expect interface{}) {
	if result != expect {
		t.Fatalf("Expect (Value: %v) (Type: %T) - Got (Value: %v) (Type: %T)", expect, expect, result, result)
	}
}

func TestSendRequestWithStatus200(t *testing.T) {
	setUp()
	defer tearDown()

	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		assertEqual(t, r.Method, "GET")
		w.WriteHeader(http.StatusOK)
	})

	req, err := request.sendRequest(server.URL)
	if err != nil {
		t.Fatalf("Error: %v", err)
	}

	assertEqual(t, req, 200)
}

func TestSendRequestWithStatusNotOK(t *testing.T) {
	setUp()
	defer tearDown()

	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		assertEqual(t, r.Method, "GET")
		w.WriteHeader(404)
	})

	req, err := request.sendRequest(server.URL)
	if err != nil {
		t.Fatalf("Error: %v", err)
	}

	assertEqual(t, req, 404)
}

func TestTypeOfTimeResponse(t *testing.T) {
	setUp()
	defer tearDown()

	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		assertEqual(t, r.Method, "GET")
		w.WriteHeader(http.StatusOK)
	})

	gitlabURL = server.URL

	i, time, _ := request.GetRequestResponseTime()
	assertEqual(t, reflect.TypeOf(i).Name(), "int")
	assertEqual(t, reflect.TypeOf(time).Name(), "Duration")
	assertEqual(t, i, 200)
}
