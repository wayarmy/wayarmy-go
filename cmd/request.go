package cmd

import (
	"net/http"
	"time"

	"github.com/pkg/errors"
)

// Request hold the request config
type Request struct {
	httpClient *http.Client
}

var gitlabURL = "https://gitlab.com"

// New create new request
func New() *Request {
	request := &Request{
		httpClient: http.DefaultClient,
	}

	return request
}

func (r *Request) sendRequest(url string) (int, error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return 0, errors.Wrap(err, "")
	}

	resp, err := r.httpClient.Do(req)
	if err != nil {
		return 0, errors.Wrap(err, "")
	}
	return resp.StatusCode, nil
}

// GetRequestResponseTime will get response time
// of request send from local to https://gitlab.com
func (r *Request) GetRequestResponseTime() (int, time.Duration, error) {
	start := time.Now()
	resp, err := r.sendRequest(gitlabURL)
	if err != nil {
		return 0, time.Since(start), errors.Wrap(err, "")
	}

	return resp, time.Since(start), nil
}
