# Wayarmy-Go

`wayarmy-go` is commandline written in go that will help you request to https://gitlab.com, get the response time, response code and show you the response time average in 1 minute.

## Installation

`go get -u gitlab.com/wayarmy/wayarmy-go`
## Usage

```
~$ wayarmy
Response: 302
Response Time: 1724.636 ms
----------------------------
Response: 302
Response Time: 1198.0919999999999 ms
----------------------------
Response: 302
Response Time: 1477.838 ms
----------------------------
Response: 302
Response Time: 1220.4060000000002 ms
----------------------------
Response: 302
Response Time: 1228.977 ms
----------------------------
Response: 302
Response Time: 1063.3870000000002 ms
----------------------------
```

## Development

Please carefully to run `setup.sh`